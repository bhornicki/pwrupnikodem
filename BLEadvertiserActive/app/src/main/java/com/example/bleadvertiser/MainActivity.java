package com.example.bleadvertiser;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {

    private static final int PERM_LOCATION = 2;
    private Button myButton;
    private Button gpsButton;
    private ParcelUuid pUuid1 = new ParcelUuid(UUID.randomUUID());
    private ParcelUuid pUuid2 = new ParcelUuid(UUID.randomUUID());

    protected LocationManager locationManager;
    TextView advTV;
    TextView locTV;
    TextView timeTV;
    TextView debTV;

    protected String bName;
    protected String locString = "loc";
    protected String timeString = "time";
    protected boolean advertising = false;

    protected boolean gpsOn = false;

    protected BluetoothLeAdvertiser advertiser;
    protected AdvertiseSettings settings;

    protected byte[] UuidData1;

    protected byte[] UuidData2;

    protected UUID myUuid;

    protected ParcelUuid myPuuid;

    protected UUID myUuid2;

    protected ParcelUuid myPuuid2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myButton = findViewById(R.id.advertisment_button);
        myButton.setOnClickListener(this);
        gpsButton = findViewById(R.id.GPS);
        gpsButton.setOnClickListener(this);
        advTV = findViewById(R.id.advertisingTextView);
        locTV = findViewById(R.id.textView1);
        timeTV = findViewById(R.id.textView2);
        debTV = findViewById(R.id.debugTV);
        gpsButton.setOnClickListener(this);
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERM_LOCATION);
        bName = Settings.Secure.getString(getContentResolver(), "bluetooth_name");
        if(bName.length() > 12)
            bName = bName.substring(0,11);
        advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();
        final BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();

        if(bAdapter == null)
        {
            Toast.makeText(getApplicationContext(),"Bluetooth Not Supported",Toast.LENGTH_SHORT).show();
        }
        else{
            if(!bAdapter.isEnabled()){
                startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),1);
                Toast.makeText(getApplicationContext(),"Bluetooth Turned ON",Toast.LENGTH_SHORT).show();
            }
        }

        settings = new AdvertiseSettings.Builder()
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
                .setConnectable(false)
                .build();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        UuidData1 = bName.getBytes();
        UuidData1[UuidData1.length - 1] = "a".getBytes()[0];

        UuidData2 = bName.getBytes();
        UuidData1[UuidData2.length - 1] = "b".getBytes()[0];

        myUuid = UUID.nameUUIDFromBytes(UuidData1);
        myPuuid = new ParcelUuid(myUuid);

        myUuid2 = UUID.nameUUIDFromBytes(UuidData2);
        myPuuid2 = new ParcelUuid(myUuid2);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.advertisment_button) {
            if(!advertising)
            {
                gpsOn = false;
                turnGpsOff();
                turnAdvOn();
                advertising = true;
            }
            else
            {
               turnAdvOff();
               turnGpsOff();
                advertising = false;
                gpsOn = false;
            }
        }

        else if (v.getId() == R.id.GPS) {
            getInfo();
            if(gpsOn)
            {
                turnAdvOff();
                turnGpsOff();
                advertising = false;
                gpsOn = false;
            }
            else
            {
                if(advertising)
                {
                    turnAdvOff();
                    turnGpsOff();
                    advertising = false;
                    gpsOn = false;
                }
                else
                {
                    advertising = true;
                    gpsOn = true;
                    turnGpsOn();
                }
            }
        }
    }

    public void turnAdvOn(){
        advertiser.stopAdvertising(advertiseCallback);
        advertise();
        locationManager.removeUpdates(this);
        advTV.setText("advertising on");
        timeTV.setText("no timestamp");
        locTV.setText("no localization");
        debTV.setText("Gps off");
    }

    public void turnAdvOff(){
        advertiser.stopAdvertising(advertiseCallback);
        advTV.setText("advertising off");
        locationManager.removeUpdates(this);
        timeTV.setText("no timestamp");
        locTV.setText("no localization");
        debTV.setText("Gps off");
    }

    public void turnGpsOn(){
        debTV.setText("Gps on");
        advertiser.stopAdvertising(advertiseCallback);
        advertise();
        advTV.setText("advertising on");
    }

    public void turnGpsOff(){
        timeTV.setText("no timestamp");
        locTV.setText("no localization");
        debTV.setText("Gps off");
        advTV.setText("advertising off");
        advertiser.stopAdvertising(advertiseCallback);
        locationManager.removeUpdates(this);
    }

    public void advertise() {

        if(!gpsOn)
        {
            byte[] myData = bName.getBytes();

            AdvertiseData data = new AdvertiseData.Builder()
                    .setIncludeDeviceName(false)
                    .addServiceData(myPuuid,  myData)
                    .build();

            byte[] myData2 = {'w', 'o', 'r', 'k', 's'};

            AdvertiseData responseData = new AdvertiseData.Builder()
                    .setIncludeDeviceName(false)
                    .addServiceData(myPuuid2,  myData2)
                    .build();

            advertiser.stopAdvertising(advertiseCallback);
            advertiser.startAdvertising(settings, data, responseData, advertiseCallback);
        }

        else
        {
            String temp = timeString;
            byte[] myData = temp.getBytes();

            AdvertiseData data = new AdvertiseData.Builder()
                    .setIncludeDeviceName(false)
                    .addServiceData(myPuuid, myData)
                    .build();

            temp = locString;
            byte[] myData2 = temp.getBytes();


            AdvertiseData responseData = new AdvertiseData.Builder()
                    .setIncludeDeviceName(false)
                    .addServiceData(myPuuid2, myData2)
                    .build();

            advertiser.stopAdvertising(advertiseCallback);
            advertiser.startAdvertising(settings, data, responseData, advertiseCallback);
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void buildAlertMessageOnStartFailure(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setCancelable(true)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void getInfo() {

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        String temp;
        locString = "w:" + location.getLatitude();
        locString = locString.substring(0, 6);
        temp = ",l:" + location.getLongitude();
        temp = temp.substring(0, 6);
        locString += temp;
        locTV.setText(locString);
        Long tsLong = System.currentTimeMillis();
        timeString = tsLong.toString();
        Date d = new Date(Long.parseLong(timeString));
        timeTV.setText(d.toString());
        advertise();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude","status");
    }

    AdvertiseCallback advertiseCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            super.onStartSuccess(settingsInEffect);
        }

        @Override
        public void onStartFailure(int errorCode) {
            Log.e("BLE", "Advertising onStartFailure: " + errorCode);
            buildAlertMessageOnStartFailure("BLE" + "Advertising onStartFailure: " + errorCode);
            super.onStartFailure(errorCode);
        }
    };
}