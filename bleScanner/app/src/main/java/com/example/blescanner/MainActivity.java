package com.example.blescanner;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private final static int ID_ENABLE_BT = 1;
    private static final int ID_PERM_LOCATION = 2;
    TextView scannerTextView;
    Button buttonScan;
    Button buttonClear;
    String initString = new String("Bluetooth Low Energy scanner\n" +
            "Labolatoria Inżynieria Oprogramowania\n" +
            "Autorzy: Bartosz Hornicki, Kacper Sarzyński\n" +
            "Prowadzący: Dr Inż. Jan Nikodem\n\n");

    BluetoothManager btManager;
    BluetoothAdapter btAdapter;
    BluetoothLeScanner btScanner;

    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            long rxTimestampMillis = System.currentTimeMillis() -
                    SystemClock.elapsedRealtime() +
                    result.getTimestampNanos() / 1000000;
            Date rxDate = new Date(rxTimestampMillis);
            String sDate = new SimpleDateFormat("HH:mm:ss.SSS").format(rxDate);

            String name = String.valueOf(result.getDevice().getName());
            String address = String.valueOf(result.getDevice().getAddress());
            String uuid = String.valueOf(result.getScanRecord().getServiceUuids());
            String data = new String("null");
            String rssi = String.valueOf(result.getRssi());
            try {
                data = new String(result.getScanRecord().getBytes(),"ASCII");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            scannerTextView.append("["+sDate+"] ");
            if(name!="null")
                scannerTextView.append("\n\tNazwa: " + name);
            if(address!="null")
                scannerTextView.append("\n\tAddr: " + address);
            if(uuid!="null")
            {
                scannerTextView.append("\n\tUUID: ");
                result.getScanRecord().getServiceUuids().forEach((el) -> scannerTextView.append("\n\t\t" + el));
            }

            scannerTextView.append("\n\tRSSI: " + rssi);

            if(result.getScanRecord().getServiceData().size()>0)
            {
                scannerTextView.append("\n\tParcelID, dane");
                result.getScanRecord().getServiceData().forEach( (k,el) ->
                {
                    scannerTextView.append("\n\t\t" + k + ", ");
                    try {
                        scannerTextView.append(new String(el,"UTF-8"));
                    } catch (UnsupportedEncodingException e) {}
                } );
            }

            if(data!="null")
            {
                scannerTextView.append("\n\tRaw Data: \"" + data +"\"");
                scannerTextView.append("(len: " + data.indexOf(0) +")");
            }
            scannerTextView.append("\n");


        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode== ID_PERM_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                scannerTextView.append("Uzyskano permisję lokalizacji\n");
            else
                scannerTextView.append("Nie uzyskano permisji lokalizacji, aplikacja może działać niepoprawnie!\n");
        }
    }
    private void buttonAction()
    {
        if(buttonScan.getText()=="Skanuj") {
            if (!btAdapter.isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, ID_ENABLE_BT);
                return;
            }
            if (btScanner == null)
                btScanner = btAdapter.getBluetoothLeScanner();

            buttonScan.setText("Zatrzymaj");
            buttonClear.setEnabled(true);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    btScanner.startScan(leScanCallback);
                }
            }).start();
        }
        else {
            buttonScan.setText("Skanuj");
            buttonClear.setEnabled(false);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    btScanner.stopScan(leScanCallback);
                }
            }).start();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonScan = (Button) findViewById(R.id.buttonScan);
        buttonScan.setText("Skanuj");
        buttonScan.setOnClickListener((v)->{buttonAction();});

        buttonClear = (Button) findViewById(R.id.buttonClear);
        buttonClear.setText("Wyczyść");
        buttonClear.setOnClickListener((v)->{scannerTextView.setText(initString);});
        buttonClear.setEnabled(false);

        scannerTextView = (TextView) findViewById(R.id.scannerTextView);
        scannerTextView.setText(initString);
        scannerTextView.setMovementMethod(new ScrollingMovementMethod());

        btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        if (!btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, ID_ENABLE_BT);
        }
        btScanner = btAdapter.getBluetoothLeScanner();

        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ID_PERM_LOCATION);
    }
}