package com.example.wifi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    int port = 4321;


    private TextView ipTextView;
    private TextView gateTextView;
    private TextView devicesTextView;
    private TextView messagesTextView;
    private TextView transmitLabelTextView;
    private TextView receivedLabelTextView;

    private Button discoverButton;
    private Button sendButton;
    private Button sensorSendButton;
    private Button clearButton;
    private TextInputEditText ipInput;
    private TextInputEditText messageInput;

    private Timer arpTimer;
    private Timer ipTimer;
    private Timer sensorTimer;
    private WifiCommon wifiCommon;

    private String sensorSendSetOn = "Włącz wysyłanie  danych z sensorów";
    private String sensorSendSetOff = "Wyłącz wysyłanie  danych z sensorów";

    double[] gravity = new double[3];
    double[] linear_acceleration = new double[3];
    float[] angular_speed = new float[3];

    final float alpha = 0.8f;

    private SensorEventListener mAccelerometerSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            Log.d("MY_APP", event.toString());
            // kod z dokumentacji
            // accelerometer
            // ---------------------------------------------------------------------------------------
            // Isolate the force of gravity with the low-pass filter.
            gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
            gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
            gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

            // Remove the gravity contribution with the high-pass filter.
            linear_acceleration[0] = event.values[0] - gravity[0];
            linear_acceleration[1] = event.values[1] - gravity[1];
            linear_acceleration[2] = event.values[2] - gravity[2];
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            Log.d("MY_APP", sensor.toString() + " - " + accuracy);
        }
    };

    private SensorEventListener mGyroscopeSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            Log.d("MY_APP", event.toString());
            angular_speed = event.values.clone();
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            Log.d("MY_APP", sensor.toString() + " - " + accuracy);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ipTextView = findViewById(R.id.ipTextView);
        gateTextView = findViewById(R.id.gateTextView);

        devicesTextView = findViewById(R.id.devicesTextView);
        messagesTextView = findViewById(R.id.receivedTextView);
        messagesTextView.setMovementMethod(new ScrollingMovementMethod());
        devicesTextView.setMovementMethod(new ScrollingMovementMethod());

        transmitLabelTextView = findViewById(R.id.transmitLabelTextView);
        receivedLabelTextView = findViewById(R.id.receivedLabelTextView);

        transmitLabelTextView.append(String.valueOf(port));
        receivedLabelTextView.append(String.valueOf(port));

        discoverButton = findViewById(R.id.discoverButton);
        sendButton = findViewById(R.id.sendButton);
        sensorSendButton = findViewById(R.id.sensorSendButton);
        clearButton = findViewById(R.id.clearButton);

        ipInput = findViewById(R.id.IPInput);
        messageInput = findViewById(R.id.messageInput);

        wifiCommon = new WifiCommon(getApplicationContext());

        ipTimer = new Timer();
        ipTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> ipTextView.setText(wifiCommon.getIP()));
                runOnUiThread(() -> gateTextView.setText(wifiCommon.getGate()));
            }
        }, 0, 1000);

        arpTimer = new Timer();
        arpTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> devicesTextView.setText(arp()));
            }
        }, 0, 2000);


        new Thread(new ReceiverThread(this, 1000, port, messagesTextView)).start();

        discoverButton.setOnClickListener((v) -> {
            new Thread(() -> {
                runOnUiThread(() -> discoverButton.setEnabled(false));
                wifiCommon.pingAll();
                runOnUiThread(() -> discoverButton.setEnabled(true));
            }).start();
        });

        sendButton.setOnClickListener((v) -> {
            String messageToSend, ipToSend;
            messageToSend = messageInput.getText().toString();
            ipToSend = ipInput.getText().toString();
            new Thread(new TransmitterThread(messageToSend, ipToSend, port)).start();
        });

        clearButton.setOnClickListener((v) -> {
            messagesTextView.setText("");
        });

        SensorManager accelerometerSensorManager =
                (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometerSensor =
                accelerometerSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

        SensorManager gyroscopeSensorManager =
                (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor gyroscopeSensor =
                gyroscopeSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        if (accelerometerSensor != null) {
            accelerometerSensorManager.registerListener(
                    mAccelerometerSensorListener, accelerometerSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }

        if (gyroscopeSensor != null) {
            gyroscopeSensorManager.registerListener(
                    mGyroscopeSensorListener, gyroscopeSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
        }

        sensorSendButton.setText(sensorSendSetOn);
        sensorSendButton.setOnClickListener((v) -> {
            if(sensorSendButton.getText() == sensorSendSetOn)
            {
                sensorTimer = new Timer();
                sensorTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        String messageToSend, ipToSend;
                        messageToSend = "Akcelerometr:\n"
                                + "x:"  + linear_acceleration[0] + "\n"
                                + "y:" + linear_acceleration[1]+ "\n"
                                + "z:"  + linear_acceleration[2]+ "\n"
                                + "Żyroskop:\n"
                                + "x:"  + angular_speed[0]+ "\n"
                                + "y:"  + angular_speed[1]+ "\n"
                                + "z:"  + angular_speed[2]+ "\n";

                        ipToSend = ipInput.getText().toString();
                        new Thread(new TransmitterThread(messageToSend, ipToSend, port)).start();
                    }
                }, 0, 50);
                sensorSendButton.setText(sensorSendSetOff);
            }
            else
            {
                sensorTimer.cancel();
                sensorSendButton.setText(sensorSendSetOn);
            }
        });
    }

    public String arp() {
        try {
            String[] shell = {"sh", "-c", "ip nei s to 0/0"};
            Process proc = Runtime.getRuntime().exec(shell);

            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null) {
                String[] strings = line.split(" +");
                if(strings.length > 4)
                    stringBuilder.append(String.format("IP: %-15s\tMAC: %17s\n", strings[0], strings[4]));
            }

            return  stringBuilder.toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}