package com.example.wifi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.TextView;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;


public class ReceiverThread implements Runnable {
    private DatagramSocket socket;
    private int buffSize;
    private final int port;
    private final TextView output;
    private final Activity parent;

    ReceiverThread(Activity activity, int buffSize, int port, TextView output) {
        this.buffSize = buffSize;
        this.port = port;
        this.output = output;
        parent = activity;
    }

    @Override
    public void run() {
        try {
            socket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
            parent.runOnUiThread(()->
                new AlertDialog.Builder(parent)
                        .setTitle("Port zajęty")
                    .setMessage("Port " + port +
                            " jest używany przez inną aplikację. Proszę ją wyłączyć i uruchomić program ponownie.")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> parent.finish())
                    .show());
            return;
        }

        while (true)
            try {
                byte[] recvBuff = new byte[buffSize];
                DatagramPacket packet = new DatagramPacket(recvBuff, buffSize);
                socket.receive(packet);

                parent.runOnUiThread(() -> {
                    String old = output.getText().toString();
                    output.setText(
                            '[' + packet.getAddress().getHostAddress() + "]:\t" +
                                    new String(packet.getData()).trim() +
                                    "\n" +
                                    old);
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
