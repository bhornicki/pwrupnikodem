package com.example.wifi;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class TransmitterThread implements Runnable {
    private DatagramSocket socket = null;
    private int port;
    private String messageToSend;
    private String  ipToSend;

    TransmitterThread(String message, String ip, int port){
        this.port = port;
        this.messageToSend = message;
        this.ipToSend = ip;
    }

    @Override
    public void run() {
        try {
            byte[] buff = messageToSend.getBytes();
            InetAddress ip = InetAddress.getByName(ipToSend);
            socket = new DatagramSocket();

            DatagramPacket packet = new DatagramPacket(buff, buff.length, ip, port);
            socket.send(packet);
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
