package com.example.wifi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class WifiCommon {
    private Context context;
    WifiManager wifiManager;
    ConnectivityManager connectivityManager;

    private  String notEnabled      = "Wifi nie jest włączone";
    private  String notConnected    = "Nie połączono z rzadną siecią Wifi";
    private  String error           = "Wystąpił błąd";

    public WifiCommon(Context context){
        this.context = context;
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    private boolean isWifiEnabled(){
        return wifiManager.isWifiEnabled();
    }

    private boolean isWifiConnected() {
        NetworkInfo net = connectivityManager.getActiveNetworkInfo();
        return (net.isConnected() && net.getType() == ConnectivityManager.TYPE_WIFI);
    }

    private String getSubnetAddress()  {

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int address = wifiInfo.getIpAddress();

         String ipString = String.format(
                "%d.%d.%d",
                (address & 0xff),
                (address >> 8 & 0xff),
                (address >> 16 & 0xff));

        return ipString;
    }
    public String getGate(){
        if(!isWifiEnabled())
            return notEnabled;

        if(!isWifiConnected())
            return notConnected;

        int ipInt = wifiManager.getDhcpInfo().gateway;
        try {
            String ipString = InetAddress.getByAddress(
                    ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(ipInt).array())
                    .getHostAddress();
            return ipString;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return error;
        }
    }

    //huh, it returns 0.0.0.0
    //Bug was reported here with won't fix (obsolete) status:
    // https://issuetracker.google.com/issues/37015180
    //So use of WifiManager is deprecated, but not marked by @Deprecated?
    public String getMask(){
        if(!isWifiEnabled())
            return notEnabled;

        if(!isWifiConnected())
            return notConnected;

        int ipInt = wifiManager.getDhcpInfo().netmask;
        try {
            String ipString = InetAddress.getByAddress(
                    ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(ipInt).array())
                    .getHostAddress();
            return ipString;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return error;
        }
    }

    public String getIP(){
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if(!isWifiEnabled())
            return notEnabled;

        if(!isWifiConnected())
            return notEnabled;

        int ipInt = wifiInfo.getIpAddress();
        String ipString;
        try {
            ipString = InetAddress.getByAddress(
                    ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(ipInt).array())
                    .getHostAddress();
            return ipString;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return error;
        }
    }

    public void pingAll(){
        String subnet = getSubnetAddress();
        for(int i=1; i<255; i++){
            String ipToPing = subnet + '.' + i;
            try {
                InetAddress.getByName(ipToPing).isReachable(100);
            } catch (IOException e) {
            }
        }
    }

}
